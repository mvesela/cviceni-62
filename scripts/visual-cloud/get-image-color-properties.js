const {
  createCanvas,
  loadImage
} = require('canvas');

/**
 * Function which get photo path and return domain rgb color
 * viz -> https://stackoverflow.com/questions/2541481/get-average-color-of-image-via-javascript
 *
 * @param {String} imgPath - path of image
 * @returns {Promise<{r: number, b: number, g: number}>}
 */
async function getAverageRGB(imgPath) {
  const resolution = 128;
  var blockSize = 5, // only visit every 5 pixels
    defaultRGB = {
      r: 0,
      g: 0,
      b: 0
    }, // for non-supporting envs
    canvas = createCanvas(resolution, resolution),
    context = canvas.getContext && canvas.getContext('2d'),
    data,
    i = -4,
    length,
    rgb = {
      r: 0,
      g: 0,
      b: 0
    },
    count = 0;

  if (!context) {
    console.log('-> Context not defined');
    return defaultRGB;
  }

  const image = await loadImage(imgPath);
  context.drawImage(image, 0, 0, resolution, resolution);
  try {
    data = context.getImageData(0, 0, resolution, resolution);
  } catch (e) {
    /* security error, img on diff domain */
    console.log('Security error, img on diff domain');
    return defaultRGB;
  }

  length = data.data.length;

  while ((i += blockSize * 4) < length) {
    ++count;
    rgb.r += data.data[i];
    rgb.g += data.data[i + 1];
    rgb.b += data.data[i + 2];
  }

  // ~~ used to floor values
  rgb.r = ~~(rgb.r / count);
  rgb.g = ~~(rgb.g / count);
  rgb.b = ~~(rgb.b / count);

  return rgb;

}

/**
 * Function to convert rgb to hsl -> https://stackoverflow.com/questions/8022885/rgb-to-hsv-color-in-javascript
 *
 * @param r - red value
 * @param g - greed value
 * @param b - blue value
 * @returns Object - object with HSL parameters
 */

function rgb2hsv(r, g, b) {
  let rabs,
    gabs,
    babs,
    rr,
    gg,
    bb,
    h,
    s,
    v,
    diff,
    diffc,
    percentRoundFn;
  rabs = r / 255;
  gabs = g / 255;
  babs = b / 255;
  v = Math.max(rabs, gabs, babs);
  diff = v - Math.min(rabs, gabs, babs);
  diffc = c => (v - c) / 6 / diff + 1 / 2;
  percentRoundFn = num => Math.round(num * 100) / 100;
  if (diff === 0) {
    h = s = 0;
  }
  else {
    s = diff / v;
    rr = diffc(rabs);
    gg = diffc(gabs);
    bb = diffc(babs);

    if (rabs === v) {
      h = bb - gg;
    }
    else if (gabs === v) {
      h = (1 / 3) + rr - bb;
    }
    else if (babs === v) {
      h = (2 / 3) + gg - rr;
    }
    if (h < 0) {
      h += 1;
    }
    else if (h > 1) {
      h -= 1;
    }
  }
  return {
    h: Math.round(h * 360),
    s: percentRoundFn(s * 100),
    v: percentRoundFn(v * 100)
  };
}

/**
 * Get color values of picture.
 * Use it -> getImageColorProperties("../../src/img/co-chteli-cerni-panteri/pic-00-1024h.jpg").then(colorObject => console.log("-> colorObject", colorObject));
 *
 * @param {String} picturePath - path to picture
 * @returns {Promise} - promise which return object of colors
 */
module.exports = getImageColorProperties = async (picturePath) => {
  const rgb = await getAverageRGB(picturePath);
  const hsv = rgb2hsv(rgb.r, rgb.g, rgb.b);
  return {
    rgb,
    hsv
  };
};
